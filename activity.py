# create a simple employee ticketing system using classes and objects.

# Create a Person class that is an abstract class that has the following methods:
#   getFullName method -
#   addRequest method 
#   checkRequest method
#   addUser method

# temp storage
members = [];

from abc import ABC
class Person(ABC):
    def __init__(self, name, requester, request):
        self.name = name;
        self.requester = requester;
        self.request = request;
    # Methods 
    def getFullName(self):
        return(f"{self.name} {self.requester}");

    def addRequest(self):
        # self.request = request;
        # return(f"{self.request}")
        return ("Request has been added");

    def getRequest(self):
        print(f'request: {self.request}');
        
    # def addUser(self, request):
        # self._request = request

# Create an Employee class from Person with the following properties and methods:
#   Properties (Make sure they are private and have getters/setters)
#       name
#       requester
#       dateRequested
#       status
#   Methods
# Abstract methods (All methods just return Strings of simple text)
#   checkRequest() - placeholder method
#   addUser() - placeholder method
#   login() - outputs "<dateRequested> has logged in"
#   logout() - outputs "<dateRequested> has logged out"

class Employee(Person):
    def __init__(self, name, requester, dateRequested, status):
        self.name = name;
        self.requester = requester;
        self.dateRequested = dateRequested;
        self.status = status;
    
    def checkRequest(self):
        pass
    def addUser(self):
        pass

    def login(self):
        return (f"{self.dateRequested} has logged in");
    
    def logout(self):
        return (f"{self.dateRequested} has logged out");

# Create a TeamLead class from Person with the following properties and methods:
#   Properties (Make sure they are private and have getters/setters)
#       name
#       requester
#       dateRequested
#       status
#   Methods
#   Abstract methods (All methods just return Strings of simple text)
#       checkRequest() - placeholder method
#       addUser() - placeholder method
#       login() - outputs "<dateRequested> has logged in"
#       logout() - outputs "<dateRequested> has logged out"
#       addMember() - adds an employee to the members list

class TeamLead(Person):
    def __init__(self, name, requester, dateRequested, status):
        self.name = name;
        self.requester = requester;
        self.dateRequested = dateRequested;
        self.status = status;
    
    def checkRequest(self):
        pass
    def addUser(self):
        pass

    def login(self):
        print(f"{self.dateRequested} has logged in");
    
    def logout(self):
        print(f"{self.dateRequested} has logged out");

    def addMember(self, paramMember = None):
        # add employee to members list logic here
        members.append(paramMember);
        return(f"{paramMember} has been added to the list.")
    # getmember
    def get_members(self):
        return members;

# Create an Admin class from Person with the following properties and methods:
#   Properties(make sure they are private and have getters/setters)
#       name
#       requester
#       dateRequested
#       status 
#   Methods
# Abstract methods (All methods just return Strings of simple text)
#       checkRequest() - placeholder method
#       addRequest() - placeholder method
#       login() - outputs "<dateRequested> has logged in"
#       logout() - outputs "<dateRequested> has logged out"
#       addUser() - outputs "New user added”

class Admin(Person):
    def __init__(self, name, requester, dateRequested, status):
        self.name = name;
        self.requester = requester;
        self.dateRequested = dateRequested;
        self.status = status;
    
    def checkRequest(self):
        pass
    def addUser(self):
        pass

    def login(self):
        print(f"{self.dateRequested} has logged in");
    
    def logout(self):
        print(f"{self.dateRequested} has logged out");

    def addUser(self):
        # add user here
        return (f"User has been added")

# Create a Request class that has the following properties and methods:
#   Properties
#       name
#       requester
#       dateRequested
#       status
#   Methods
#       updateRequest
#       closeRequest
#       cancelRequest

class Request():
    def __init__(self, name, requester, dateRequested, status = None):
        self.name = name;
        self.requester = requester;
        self.dateRequested = dateRequested;
        self.status = status; 
    
    def udateRequest(self):
        pass
    def closeRequest(self):
        return (f"{self.status}")
    def cancelRequest(self):
        pass
    def set_status(self, status):
        self.status = status;
        # return (f"{self.status}")
        


# Test cases:
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.get_members():
    print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())